# Windows PowerShell Profile 2025

Ideas and Public Availability for Trainees and everybody else.

    You can check out Pre-2025 WindowsPowerShell Profile with git checkout v1.0

    Please do this outside of WindowsPowerShell - Git Bash for example

Ideas for Rebuild of WindowsPowerShell Profile

*   Slim down the use of Modules to speed up launch of WindowsPowerShell 
    with factor 5 to 10 (!)

*   Cross-Platform Avaibility: Provide CLI Tools with support for various OS/Shell-Platforms

*   Use `.env` file for your Profile-Configuration without changing the $PROFILE file
    which is not a good idea because of Git-Status for $PROFILE

    ``` 
    JBShowHeader=OFF
    JBMessages= OFF
    JBPowerlinePrompt=starship
    ```

*   Later/To-Do: Everything switchable. `jb_tools` vs. `jb_tools_linux`



## jb_tools (Folder for CLI Tools)

*   **OhMyPosh** - previously via Module - now: Standalone - 
    [OhMyPosh Website](https://ohmyposh.dev/) - 
    [OhMyPosh GitHub](https://github.com/jandedobbeleer/oh-my-posh) - 
    [OhMyPosh Releases](https://github.com/JanDeDobbeleer/oh-my-posh/releases)

    Themes: `get-poshThemes -Path .\jb_tools\posh-themes\` or look online via
    [OhMyPosh Themes](https://ohmyposh.dev/docs/themes)

*   **Zoxide** - `z` the better cd / autojump / Zlocation with FZF (`zi`) - 
    [Website Zoxide](https://crates.io/crates/zoxide) - [Zoxide GitHub](https://github.com/ajeetdsouza/zoxide)

    Subfolder with Zoxide Tech

*   **Vifm** - Vi-like File Manager with Theming, Previews and  - 
    [Vifm Website](https://vifm.info/) - [Vifm GitHub](https://github.com/vifm/vifm)

*   **FZF** - Fuzzy Finder - 
    Single File: `fzf.exe` - [FZF GitHub](https://github.com/junegunn/fzf)

*   **Eza** - better ls with Icons, Trees and Git Support - Single File: `eza.exe`
    [Website eza.rocks](https://eza.rocks/) - [Eza GitHub](https://github.com/eza-community/eza)

    Theming / Config: `.\eza\theme.yml` (Dummy / Empty)

*   **Bat** - better cat - Single File: `bat.exe` - in Debian `batcat` (!) _ 
    [Bat GitHub](https://github.com/sharkdp/bat)

*   **Fastfetch** - the modern Neofetch for every OS/Shell - 
    [Fastfetch GitHub](https://github.com/fastfetch-cli/fastfetch)

*   **JPEGView** - the standalone exe from Project [GitHub JPEGView](https://github.com/sylikc/jpegview)

*   **7z** - single exe from 7-Zip Tools [Website 7-zip.org](https://www.7-zip.org/)

*   **starship.toml** - prepared `starship.toml` for later Starship Installation -
    [Starship Website](https://starship.rs)

## jb_fonts (Folder for Fonts)

Easy Install on Windows via Double Click. 
Easy Testing of NF/Icon-Support with `nerdfont.txt` file in Folder `jb_fonts`.

Fonts:

*   Cascadia Code NF (Microsoft!)
*   Firacode NF (Nerdfonts.com)
*   MesloLGS NF (Support via Linux Community)
*   Sauce Code Pro NF (Nerdfonts.com - namely/originally :Source Code)

ToDo: Script for Installation - see [Gist - anthonyeden/FontInstallation.ps1](https://gist.github.com/anthonyeden/0088b07de8951403a643a8485af2709b)

```
$SourceDir   = "<your path to fonts>"
$Destination = (New-Object -ComObject Shell.Application).Namespace(0x14)
Get-ChildItem -Path $SourceDir -Include '*.ttf','*.ttc','*.otf' -Recurse | ForEach {
    # Install font
    $Destination.CopyHere($_.FullName,0x10)
}
```

## Windows Terminal 

The standard Terminal Emulator for Windows Environments / OS.

Please feel free to use the following Snippets - 
but as always: don't just copy and paste - Think!

### NordTheme

Put it into `schemes` Chapter of `settings.json` or use **One Half Dark** Standard-Theme.

The following Code shows also the Profile Entry for Windows PowerShell 
with a Background Pic provided in the jb_examples Folder.

```
        "list": 
        [
            {
                "colorScheme": "nord",
                "font": 
                {
                    "face": "Cascadia Code NF",
                    "size": 13
                },
                "guid": "{61c54bbd-c2c6-5271-96e7-009a87ff44bf}",
                "hidden": false,
                "name": "Windows PowerShell",
                "startingDirectory": "%USERPROFILE%\\Documents\\git-projekte",
				"backgroundImage" : "%USERPROFILE%\\Documents\\WindowsPowerShell\\jb_examples\\Backgrounds\\nord_valley.png",
				"backgroundImageOpacity" : 0.10,
				"backgroundImageStretchMode" : "fill"
            },



    "schemes": 
    [
        {
            "background": "#2E3440",
            "black": "#2E3440",
            "blue": "#81A1C1",
            "brightBlack": "#4C566A",
            "brightBlue": "#81A1C1",
            "brightCyan": "#8FBCBB",
            "brightGreen": "#A3BE8C",
            "brightPurple": "#B48EAD",
            "brightRed": "#BF616A",
            "brightWhite": "#ECEFF4",
            "brightYellow": "#EBCB8B",
            "cursorColor": "#ECEFF4",
            "cyan": "#88C0D0",
            "foreground": "#D8DEE9",
            "green": "#A3BE8C",
            "name": "nord",
            "purple": "#B48EAD",
            "red": "#BF616A",
            "selectionBackground": "#ECEFF4",
            "white": "#E5E9F0",
            "yellow": "#EBCB8B"
        },

        ...

    ],    
```

Example Path: `C:\Users\USERNAME\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json`

## VS Code - Example settings.json 

Open with Ctrl+Shift+P - Preferences: Open User Settings (JSON)

See also Folder `jb_examples\VSCODE_USER_AppData-Roaming-Code-User`

```
{
    "workbench.colorTheme": "Nord",
    "editor.fontFamily": "'Cascadia Code NF', 'Firacode NF', 'MesloLGS NF', Consolas, 'Courier New', monospace",
    "editor.fontSize": 16,
    "terminal.integrated.fontSize": 14,
    "editor.minimap.enabled": false,

    "update.mode": "none",
    "update.enableWindowsBackgroundUpdates": false,
    "telemetry.telemetryLevel": "off",

    "terminal.integrated.profiles.windows": {
        "PowerShell 7": {
            "source": "PowerShell",
            "icon": "terminal-powershell"
        },
        "PowerShell 5": {
            "path": "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe",
            "icon": "terminal-powershell"
        },        
        "Command Prompt": {
            "path": [
                "${env:windir}\\System32\\cmd.exe"
            ],
            "args": [],
            "icon": "terminal-cmd"
        },
        "Git Bash": {
            "source": "Git Bash"
        }
    },
    "terminal.integrated.defaultProfile.windows": "PowerShell 5",
    
    "js-unicode-preview.languages": [
        "json",
        "jsonc",
        "javascript",
        "javascriptreact",
        "typescript",
        "typescriptreact"
    ]
}
```


Have fun - keep on PowerShell-ing

Joe Brandes - 2025

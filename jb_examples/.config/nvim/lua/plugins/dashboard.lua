return {
	{
		'nvimdev/dashboard-nvim',
		event = 'VimEnter',
		config = function()
			require('dashboard').setup {
				-- config
				theme = 'hyper', -- hyper or doom
				shortcut_type = 'letter', -- letter or number
				-- disable_move = 'true',           -- true or false
				-- hide = {
				--   statusline = 'false',
				-- },

				-- my config --

				config = {

					header = {
						-- do some Ascii-Art
						'',
						'    .--.                                                         ',
						'   |o_o |      ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗',
						'   |:_/ |      ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║',
						'  ||    )      ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║',
						' (|     | )    ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║',
						'| |_   _|`|    ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║',
						'|___)=(___|    ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝',
						'',
					},
					week_header = {
						enable = false, -- with true it becomes the displayed header
					},
					shortcut = {
						{
							desc = '📦 Lazy Updates ',
							group = '@property',
							action = 'Lazy update',
							key = 'u'
						},
						{
							-- icon = '',
							-- icon_hl = '@variable',
							desc = '📃 Files ',
							group = 'Label',
							action = 'Telescope find_files',
							key = 'f',
						},
						{
							desc = '🏷️ Help Tags ',
							group = 'DiagnosticHint',
							action = 'Telescope help_tags',
							key = 'h',
						},
						{
							desc = '🛠️ Find Text ',
							group = 'Number',
							action = 'Telescope live_grep',
							key = 'g',
						},
					},
					footer = {
						"",
						"📢 Hello Joe - Have fun with Neovim and the Console Magic 📢",
						"",
					},
				},

			} -- setup end
		end, -- function for config end

		dependencies = { { 'nvim-tree/nvim-web-devicons' } }
	}
}

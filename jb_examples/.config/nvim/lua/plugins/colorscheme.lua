return {
  -- CATPPUCCIN --
  { 
    "catppuccin/nvim", 
    name = "catppuccin",
    lazy = false, 
    priority = 1000,
    -- config = function()
    --  vim.cmd([[colorscheme catppuccin-frappe]])
    -- end,
  },

  -- TOKYONIGHT --
  {
    "folke/tokyonight.nvim",
    lazy = false, -- make sure we load this during startup if it is your main colorscheme
    priority = 1000, -- make sure to load this before all the other start plugins
    -- config = function()
    --   -- load the colorscheme here
    --   vim.cmd([[colorscheme tokyonight]])
    -- end,
  },


  -- NORD --
  {
    "shaunsingh/nord.nvim",
    lazy = false, -- make sure we load this during startup if it is your main colorscheme
    priority = 1000, -- make sure to load this before all the other start plugins
    config = function()
    -- load the colorscheme here
       vim.cmd([[colorscheme nord]])
    end,
  },




}

# vim:foldmethod=marker
# ============================================================================
#       _            ____    _____  _____   ____  ______ _____ _      ______
#      | |          |  _ \  |  __ \|  __ \ / __ \|  ____|_   _| |    |  ____|
#      | | ___   ___| |_) | | |__) | |__) | |  | | |__    | | | |    | |__
#  _   | |/ _ \ / _ \  _ <  |  ___/|  _  /| |  | |  __|   | | | |    |  __|
# | |__| | (_) |  __/ |_) | | |    | | \ \| |__| | |     _| |_| |____| |____
#  \____/ \___/ \___|____/  |_|    |_|  \_\\____/|_|    |_____|______|______|
#
# ============================================================================
# Obviously make PowerShell Scripts *.ps1 executable
# Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
# ============================================================================
# Author:            Joe Brandes
# Version:           2024-12
# PowerShell:        pwsh on Windows
# ============================================================================

# ============================================================================
# Configurations
# ============================================================================
# {{{

# Path for JoeB Tools - no hard coded Path - always use $HOME instead!
# MyOwn Path trailing all other Paths!
# -------------------
$JBTools   = "$PSScriptRoot\jb_tools"
$env:PATH += ";$JBTools"

# Configs (Variables for Profile / Colors)
# -------
# external Pre-Configuration possible via gitignored .env file
# Example:
# JBShowHeader=ON
# JBMessages= ON
# JBPowerlinePrompt=ohmyposh
# 
# Profile will check for file and then preload Varible Values
if (Test-Path -Path "$PSScriptRoot\.env") {
    $vars = Get-Content -Path "$PSScriptRoot\.env" 
    foreach ($var in $vars) {
	$varname = $var.split("=")[0]
	$varvalue = $var.split("=")[1]
	Set-Variable -Name $varname -Value $varvalue
    }
} else {
    # header with ASCII - Show: ON | other value OFF
    $JBShowHeader           = "ON"
    # Configure Messages ON | OFF (aka not ON)
    $JBMessages             = "ON"
    # Configure Powerlineprompt: ohmyposh | starship
    $JBPowerlinePrompt      = "ohmyposh"
}

# Configure Color for Modules - see [System.ConsoleColor] static Properties
# Blue, Cyan, Gray, Green, Magenta, Red (all also as Dark...), Black, White
$JBHeaderColor          = "White"
$JBModuleColor          = "Yellow"
$JBSpecialColor         = "Cyan"
# Configure Color for Messages
If ( $JBMessages -eq "ON" ) {
	$JBFgColor           = "Magenta"
} else {
	$JBFgColor           = "Green"
}

# lines for outputs
# $TempLine = "================================================================================="
# $TempLine = "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
$TempLine = "---------------------------------------------------------------------------------"

# ============================================================================
# }}}

# ============================================================================
# Greatings
# ============================================================================
# {{{

Clear-Host

# Header with ASCII Graphics
if ($JBShowHeader -eq "ON") {
	Write-Host -ForegroundColor $JBHeaderColor $(Get-Content -Path $PSScriptRoot\ProfileHeader.txt -Raw)
}

# Main Infos - always on
Write-Host -ForegroundColor $JBFgColor "# $TempLine #`n# " -NoNewline
Write-Host                             "Config Variables    Header: " -NoNewline
Write-Host -ForegroundColor $JBFgColor "$JBShowHeader " -NoNewline
Write-Host                             "       Messages: " -NoNewline
Write-Host -ForegroundColor $JBFgColor "$JBMessages " -NoNewline
Write-Host                             "       Prompt: " -NoNewline
Write-Host -ForegroundColor $JBFgColor "$JBPowerlinePrompt       "
Write-Host -ForegroundColor $JBFgColor "# $TempLine #"

if ($JBMessages -eq "ON") {
	Write-Host "# Prompt/PowerLines   ohmyposh (with Git/Oh-My-Posh) or starship (Starship.rs)      #"
}

# ============================================================================
# }}}

# ============================================================================
# Aliase
# ============================================================================
# {{{

# Notepad++ 
# Path for 32-Bit Notepad++ Version - if you need Plugin Support)
# $np = "C:\Program Files (x86)\Notepad++\notepad++.exe"
# ---------
$np = "C:\Program Files\Notepad++\notepad++.exe"
if ( Test-Path $np ) {
	Set-Alias notepad $np
	Set-Alias notepad.exe $np
	Set-Alias edit $np
	Set-Alias np $np
}

# Linux confused ;-)
# ------------------
# Set-Alias ll Get-ChildItem

# Midnight Commander for Win
# --------------------------
# https://sourceforge.net/projects/mcwin32/ 
# Set-Alias mc "C:\programme_ni\mcwin32-build204-bin\mc.exe"

# pdfreader x-change-viewer
# -------------------------
#Set-Alias pdfreader "C:\programme_ni\PortableApps\PDF-XChangeViewerPortable\PDF-XChangeViewerPortable.exe"

# Vim has to be moved after posh-git module, because there comes a vim with git 
# e.g. / look at Get-Command vim and Get-Command vim.exe
# C:\Users\dozib\AppData\Local\Programs\Git\usr\bin\vim.exe
# Set-Alias vim "C:\programme_ni\PortableApps\gVimPortable\App\vim\vim80\vim.exe"
# Set-Alias vim.exe "C:\programme_ni\PortableApps\gVimPortable\App\vim\vim80\vim.exe"
# Set-Alias vim vim.exe

# famous last words
# -----------------

# ============================================================================
# }}}



# ============================================================================
# Modules
# ============================================================================
# {{{

# Oh-My-Posh (needs posh-git): PowerShell powerline-go stuff
# ----------------------------------------------------------
# # see: https://docs.microsoft.com/de-de/windows/terminal/tutorials/powerline-setup
# do: (or have the Modules you need as folders)
# Install-Module posh-git -Scope CurrentUser
# oh-my-posh is new for every OS/Shell: https://ohmyposh.dev/
# Use PowerLine stuff - please comment Starship.rs stuff
# ------------------------------------------------------
#
# Infos to PowerLine/Oh-My-Posh OR Starship Prompts/PowerLines
# Variable $JBPowerlinePrompt: ohmyposh | starship

if ( $null -ne $($env:Path | Select-String -Pattern 'Git\\cmd') -and $JBPowerlinePrompt -eq "ohmyposh") 
{ 
	Import-Module posh-git
	# Set Aliase and Variables
	Set-Alias oh-my-posh "posh-windows-amd64.exe"
	$poshthemes = "$PSScriptRoot\jb_tools\posh-themes"
	# Themes: https://ohmyposh.dev/docs/themes
	$theme = "jb-nord"
	# Call Oh-My-Posh Standalone Tool with Config - oh-my-posh for various OS/Shells
	oh-my-posh init powershell --config "$poshthemes\$theme.omp.json" | Invoke-Expression
	# Set-PoshPrompt -Theme paradox
	# Messages  
	if ($JBMessages -eq "ON") {
		Write-Host -Foreground      $JBFgColor       "# I found Git\cmd Path and imported Module " -NoNewline
		Write-Host -ForegroundColor $JBModuleColor   "Posh-Git" -NoNewline 
		Write-Host -ForegroundColor $JBFgColor       " and " -NoNewline 
		Write-Host -ForegroundColor $JBModuleColor   "Oh-My-Posh Exe" -NoNewline
		Write-Host -ForegroundColor $JBFgColor       " as Prompt.   #"
		Write-Host -Foreground      $JBFgColor       "# Use NerdFont (e.g. Cascadia Code NF / MesloLGS, FiraCode, ...) for Icons/Symbols! #"
		Write-Host -Foreground      $JBSpecialColor  "# Changed Module Oh-My-Posh to App: more Themes and Platform (OS / Shell) freedom!  #"
		Write-Host -ForegroundColor $JBSpecialColor  "# For more Infos look at https://ohmyposh.dev/docs/migrating (Problem statement)!   #"
		Write-Host -ForegroundColor $JBFgColor       "# $TempLine #"
	}
}

# Use Starship.rs  (Admin Privileges needed!)
# please comment PowerLine stuff (above: posh-git + oh-my-posh)
# ----------------------------------------------------------------------------
# Config:                        ~/.config/starship.toml
# needs to be installed (e.g.):  winget install --id Starship.Starship
# or with Chocolatey:            choco install starship
#
if ($JBPowerlinePrompt -eq "starship") {
	Invoke-Expression (&starship init powershell)
}
if ($JBMessages -eq "ON" -and $JBPowerlinePrompt -eq "starship") { 
	Write-Host -ForegroundColor     $JBFgColor       "# This Profile uses Starship.rs as Prompt/PowerLine - use NerdFonts for Icons!      #"
	Write-Host -ForegroundColor     $JBFgColor       "# $TempLine #"
}
 

# Header for more Infos: Tool and Modules
# ---------------------------------------
if ($JBMessages -eq "ON") {
	Write-Host         "# Infos for Software-Tools (CLI-Tools) and Modules:                                 #"
        Write-Host -ForegroundColor $JBFgColor       "# $TempLine #"
}

# Use Zoxide instead of Module ZLocation
# ---------

# make zoxide a Command/Alias
Set-Alias zoxide "$JBTools\zoxide-0.9.6-x86_64-pc-windows-msvc\zoxide.exe"

# provide for Tools z and zi (with FZF)
Invoke-Expression (& { (zoxide init powershell | Out-String) })

if ($JBMessages -eq "ON") {
	Write-Host                                      "# " -NoNewline 
	Write-Host -ForegroundColor     $JBModuleColor  "Zoxide" -NoNewline 
	Write-Host                                      "              z, zi - The Tool for better use of Change Dir (with FZF)      #"
}

# FZF and Ripgrep
if ($JBMessages -eq "ON") {
	Write-Host                                      "# " -NoNewline 
	Write-Host -ForegroundColor     $JBModuleColor  "FZF, RG" -NoNewline 
	Write-Host                                      "             Fuzzy Finder, Ripgrep - The Standards for efficient Searches  #"
}

# Eza
# ---
# Get Colors and Icons going

$env:EZA_CONFIG_DIR = "$JBTools\eza"
function ll {
	param (
		[string] $Path = "."
	)
	eza -l --icons always --git --all --group-directories-first --no-symlinks $Path
}

if ($JBMessages -eq "ON") {
	Write-Host                                      "# " -NoNewline 
	Write-Host -ForegroundColor     $JBModuleColor  "Eza" -NoNewline 
	Write-Host                                      "                 Eza - The clever Tool to ls and tree with icons and more      #"
}
# vifm - VI File Manager
# ----

# $MYVIFMRC = "$JBTools\vifm-w64-se-0.13-binary\Vifm\vimrc"
# $MYVIFMRC
Set-Alias vifm "$JBTools\vifm-w64-se-0.13-binary\vifm.exe"

if ($JBMessages -eq "ON") {
	Write-Host                                      "# " -NoNewline 
	Write-Host -ForegroundColor     $JBModuleColor  "Vifm" -NoNewline 
	Write-Host                                      "                Vi File Manager - managing Files and Folders in the terminal  #"
}

# fastfetch - infos for the terminal
# ---------

Set-Alias fastfetch "$JBTools\fastfetch-windows-amd64\fastfetch.exe"

if ($JBMessages -eq "ON") {
	Write-Host                                      "# " -NoNewline 
	Write-Host -ForegroundColor     $JBModuleColor  "Fastfetch" -NoNewline 
	Write-Host                                      "           Neofetch Alternative - there is no Terminal without it ;-)    #"
}


# Neovim for Windows - nerdy freaky
# ------------------
# $nvimqtexe = "C:\programme_ni\nvim-win64\bin\nvim-qt.exe"
$nvimexe   = "C:\programme_ni\nvim-win64\bin\nvim.exe"
$nvimpath  = "C:\programme_ni\nvim-win64\bin"
# if ( (Test-Path $nvimqtexe) -and (Test-Path $nvimexe) ) {
if (Test-Path $nvimexe) {	
	# Set-Alias nvim $nvimexe # Alias not working in vifm Shells
	$env:PATH += ";$nvimpath"
} 

if ($JBMessages -eq "ON") {
	Write-Host                                      "# " -NoNewline
	Write-Host -ForegroundColor     $JBModuleColor  "Neovim" -NoNewline 
	Write-Host                                      "              Text and Coding - Easy Install-Script in .\jb_examples        #"
}


# show delimiter between CLI-Tools and Modules
if ($JBMessages -eq "ON") {
        Write-Host -ForegroundColor $JBFgColor       "# $TempLine #"
}

# PSFzf 
# -----
# Windows Hilfe: https://github.com/kelleyma49/PSFzf
# Windows Fzf Binary in .\Documents\WindowsPowerShell\tools
#Import-Module PSFzf
# replace 'Ctrl+t' and 'Ctrl+r' with your preferred bindings:
Set-PsFzfOption -PSReadlineChordProvider 'Ctrl+t' -PSReadlineChordReverseHistory 'Ctrl+r'

if ($JBMessages -eq "ON") {
	Write-Host                                      "# " -NoNewline 
	Write-Host -ForegroundColor     $JBModuleColor  "PSFZF" -NoNewline 
	Write-Host                                      "               integrate CLI-Tools FZF and BAT (see jb_tools and Path)       #"
}

# Ripgrep
# -------
# Config for ripgrep Tool
$env:RIPGREP_CONFIG_PATH = "$HOME\Documents\WindowsPowerShell\jb_tools\.config\.ripgreprc"

# PSReadline
# ----------
# provide newer Version of PSReadline (here: 2.2.6)
# for Predictive IntelliSense aka Autosuggestions (like Fish or ZSH)
# e.g.: https://khushwant.hashnode.dev/powershell-autosuggestions-like-fishzsh-autosuggestions
Set-PSReadLineOption -PredictionSource History
Set-PSReadLineOption -HistorySearchCursorMovesToEnd	
Set-PSReadLineKeyHandler -Key UpArrow -Function HistorySearchBackward
Set-PSReadLineKeyHandler -Key DownArrow -Function HistorySearchForward
Set-PSReadLineOption -Colors @{ InlinePrediction = '#875f5f'}
Set-PSReadLineKeyHandler -Chord "Ctrl+RightArrow" -Function ForwardWord

if ($JBMessages -eq "ON") {
	Write-Host                                      "# " -NoNewline 
	Write-Host -ForegroundColor     $JBModuleColor  "PSReadline" -NoNewline 
	Write-Host                                      "          newer Version (old: 2.0.0) for AutoSuggestions like ZSH/Fish  #"
}


# Docker Support 
# --------------
# Modul: https://github.com/matt9ucci/DockerCompletion
# Install-Module DockerCompletion -Scope CurrentUser 
# 
#Import-Module DockerCompletion

# More Modules provides by this WindowsPowerShell Git Repo
# --------------------------------------------------------

# PackageManagement
if ($JBMessages -eq "ON") {
	Write-Host                                      "# " -NoNewline 
	Write-Host -ForegroundColor     $JBModuleColor  "PackageManagement" -NoNewline 
	Write-Host                                      "   newer Version (old: 1.0.0.1) for Package handling             #"
}


# VirtualDesktops
if ($JBMessages -eq "ON") {
	Write-Host                                      "# " -NoNewline 
	Write-Host -ForegroundColor     $JBModuleColor  "VirtualDesktop" -NoNewline 
	Write-Host                                      "      for Windows Virtual Desktops (see folder jb_examples)         #"
}


# Last LINE
if ($JBMessages -eq "ON") {
        Write-Host -ForegroundColor $JBFgColor       "# $TempLine #`n"
}


# ============================================================================
# }}}


# ============================================================================
# Functions
# ============================================================================
# {{{

# PSFzf functions
# ---------------
# To change to a user selected directory:
function fzdir {
	Get-ChildItem . -Recurse -Attributes Directory | Invoke-Fzf | Set-Location
}
# To edit a file:
function fzfile {
	Get-ChildItem . -Recurse -File | Invoke-Fzf | ForEach-Object { notepad $_ }
}

# FZF including preview with bat
# Bat: https://github.com/sharkdp/bat
function fzfileprev {
	Get-ChildItem . -Recurse -File | 
		Invoke-Fzf -height 75% -preview 'bat --color=always --style=numbers --line-range=:500 {}' | 
		ForEach-Object { notepad $_ }
}


# Git functions
# -------------

function gst {
	git status
}

function ga {
	git add .
}


# Helper function(s)
# ------------------
# For more: please build your own Modules as shown in my Seminars!

# Import my Modules
Import-Module -Name JBTools

# ============================================================================
# }}}
